module "pipelines" {
  count  = length(var.pipelines)
  source = "./scheduled_pipeline"

  project  = gitlab_project.instance.id
  pipeline = var.pipelines[count.index]
}
