resource "google_service_account_key" "pipeline" {
  service_account_id = google_service_account.pipeline.name
}

resource "google_service_account" "pipeline" {
  account_id   = local.service_account_name
  display_name = "CI - ${var.name}"
}

resource "google_project_iam_member" "pipeline_roles" {
  count   = length(var.pipeline_iam_roles)
  project = var.project
  role    = var.pipeline_iam_roles[count.index]
  member  = "serviceAccount:${google_service_account.pipeline.email}"
}

locals {
  rfc1035_name = replace(lower(var.name), " ", "-")

  service_account_name = format("ci-%s", length(local.rfc1035_name) > 25 ? trimsuffix(substr(local.rfc1035_name, 0, 25), "-") : local.rfc1035_name)
}
