resource "gitlab_project" "instance" {
  name                   = var.name
  path                   = var.path
  namespace_id           = var.namespace_id
  description            = var.description
  visibility_level       = var.visibility_level
  shared_runners_enabled = var.shared_runners_enabled
  default_branch         = var.default_branch

  remove_source_branch_after_merge      = true
  only_allow_merge_if_pipeline_succeeds = true

  lifecycle {
    ignore_changes = [
      default_branch,
    ]
  }
}
