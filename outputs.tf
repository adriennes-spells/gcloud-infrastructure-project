output "clone_url_ssh" {
  value = gitlab_project.instance.ssh_url_to_repo
}

output "terraform_state_path" {
  value = replace(replace(replace(gitlab_project.instance.ssh_url_to_repo,
    "git@", ""),
    ".git", ""),
  ":", "/")
}
