# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "3.8.0"
  constraints = ">= 3.7.0"
  hashes = [
    "h1:ESZehCeHY3G5tCrL47HH+0W18Mt/IYslwaadGfpucU8=",
    "zh:02d977e6803ff336054194e567fcd11d095846346a604017afef98ada0353bc1",
    "zh:07294f6061db86c1ed44e5eae01576d76c585128e334761feb01fca5b866d53f",
    "zh:1cfa4cbbdb9a8a235bb82e7f3968982993750fc5a95bd948b9dfd6de772bff0a",
    "zh:3f9172206c00de224e3e234503681c29f918e2b83dbf5b11c0e791694600bec6",
    "zh:71174040a680be48f65b63fb5571bdd76cd17de830c88b81b6e684b56d77616c",
    "zh:81b1c86b8b59c4f3faed59d461555ce53d86b12f9fc91a2afaee89ca3992f154",
    "zh:a09d2012b4392c01a310713c170db1d7e211190b0e2a27e43ec2f7ca59b6d6c9",
    "zh:a97ed255d3d161313370fc34e83018216dcf198e7a5b285abe31b26bf8b048f3",
    "zh:abfad232ec29ecea620e087d20f1586d0b5b4fc02a6369a6b0c65e80b8de3566",
    "zh:c8814ae34933a082bf68db958ed6ef708321093ce9901a11e2efceaa5b3ed031",
    "zh:e5a52eaada2797fbec6aca19bc7a74cdb40dc1625df999f6a93c34690ea2bc80",
    "zh:f4e1787f9fcab92824dad41486eaa6895a789c2d25f3d3f3126d134bd5edfe90",
    "zh:f90b196e6ff444fe721603765a5be5778bc92d45458009bc77ee6d1b6c32bf67",
  ]
}

provider "registry.terraform.io/hashicorp/google" {
  version     = "4.5.0"
  constraints = ">= 3.82.0"
  hashes = [
    "h1:MWHx6G+emVNvPyxxLCpEEco5F7eoRzX9Tp9lbtO/PmI=",
    "zh:0db8d3e7cc68ddd70a75c72334908b60a875062162c5e8a78bb9e7c0060def64",
    "zh:2ed5ac0301e6382589353402b11aed8ded08bfad425016a508523e18e8766424",
    "zh:3abcaf9439bc60b7666cfc257af8bbe555a90b9692972bc46e84b2d96189bf63",
    "zh:4d9da72347c0bc8dd34fa40ae5c26c7c862dc633e7a5da89564f8fa29d950a49",
    "zh:5d4d85fb7318df9572e273ee3808c009db7856165a3f1d684e635cec67e4b0ef",
    "zh:5eac81865b2e1467e2f54e31f8766b098baee9d86e9eeda0f9608f9dbf1863b9",
    "zh:68c5095136b810a3b80d8aa698252eea4f391500904e4717f5b735f60e04388b",
    "zh:99c584e796068899cc217d8ecb2204684f851b091a4ab8442e0d9186074f54ca",
    "zh:a64e291acdd8d61c456073c91abe6b578760ee4526258d416aa5daaa3ed651b3",
    "zh:acb7844d2a9bac8213764d755fdaeb64e96b50bf11381a66e0300bc4b1aa8040",
    "zh:f294fd86cfeeee4bef2de31afd7af608d1c7f3f9e35f74127c39b02f58addc4c",
  ]
}
