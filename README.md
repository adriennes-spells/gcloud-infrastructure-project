# Google Cloud Infrastructure Project

Terraform module for scaffolding Packer or Terraform projects in GitLab. CI/CD is preconfigured to work with an IAM service account.

## Full usage example

```
module "bounday_google" {
  source = "git::git@gitlab.com:adriennes-spells/gcloud-infrastructure-project.git"

  name               = "Boundary Google"
  description        = "Boundary implemented on Google Cloud Platform"
  namespace_id       = gitlab_group.spells.id
  service_account_id = "gitlab-terraform"
  env_vars = {
    TF_VAR_region             = "us-west1"
    TF_VAR_zone               = "us-west1-a"
    TF_VAR_network            = "default"
    TF_VAR_subnetwork         = "default"
    TF_VAR_instance_count     = 3
    TF_VAR_cluster_name       = "asgard-boundary"
    TF_VAR_dns_managed_zone   = "adriennecohea-io"
  }
  file_vars = {
    "TF_VAR_ca_file"             = file("${path.module}/ca.pem")
  }
  pipelines = [
    {
      description = "Create"
      ref         = "main"
      cron        = "0 1 * * *"
      variables   = {}
    },
    {
      description = "Destroy"
      ref         = "main"
      cron        = "0 5 * * *"
      variables = {
        "DESTROY" = true
      }
    },
  ]
}
```
