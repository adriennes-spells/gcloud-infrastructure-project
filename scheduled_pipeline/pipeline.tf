resource "gitlab_pipeline_schedule" "pipeline" {
  project     = var.project
  description = var.pipeline.description
  ref         = var.pipeline.ref
  cron        = var.pipeline.cron
  active      = var.pipeline.active
}

resource "gitlab_pipeline_schedule_variable" "pipeline" {
  for_each = var.pipeline.variables

  project              = var.project
  pipeline_schedule_id = gitlab_pipeline_schedule.pipeline.id
  key                  = each.key
  value                = each.value
}
