variable "pipeline" {
  type = object({
    description = string,
    ref         = string,
    cron        = string,
    variables   = map(string),
    active      = bool
  })
}

variable "project" {
  type = number
}
